﻿using CrazyBooks.Lib.UI;
using System;
using WpfApp1.MVVM1.Views;

namespace WpfApp1.MVVM1.ViewModels
{
    public class MainViewModel: ViewModelBase
    {
        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
                NotifyPropertyChanged();
            }
        }
        string _title;
        
        public MainViewModel()
        {
            Title = "Bienvenidos a CrazyBooks";
        }
    }
}
