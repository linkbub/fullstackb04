﻿using CrazyBooks.Lib.Models;
using CrazyBooks.Lib.Services;
using CrazyBooks.Lib.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace WpfApp1.MVVM1.ViewModels
{
    public class HomeViewModel: ViewModelBase
    {
        public ISwitcher Switcher { get; set; }
        public ILoginService LoginSvc { get; set; }
        public IBooksService BooksSvc { get; set; }

        public string CurrentUserName
        {
            get
            {
                return LoginSvc.CurrentUser.Name;
            }
        }

        public string BookTitle
        {
            get
            {
                return _bookTitle;
            }
            set
            {
                _bookTitle = value;
                NotifyPropertyChanged();
            }
        }
        string _bookTitle;

        public string AuthorName
        {
            get
            {
                return _authorName;
            }
            set
            {
                _authorName = value;
                NotifyPropertyChanged();
            }
        }
        string _authorName;

        public List<Book> Lends
        {
            get
            {
                return _lends;
            }
            set
            {
                _lends = value;
                NotifyPropertyChanged();
            }
        }
        List<Book> _lends;

        public HomeViewModel(ILoginService loginSvc, IBooksService booksSvc)
        {
            LoginSvc = loginSvc;
            BooksSvc = booksSvc;

            AddBookCommand = new RouteCommand(AddBook);

            InitBooks();
        }

        public void AddBook()
        {
            var book = new Book
            {
                Id = Guid.NewGuid(),
                Title = BookTitle,
                Author = AuthorName,
                UserId = LoginSvc.CurrentUser.Id
            };
            
            if (BooksSvc.Add(book))
                Lends = BooksSvc.GetAllByUserId(LoginSvc.CurrentUser.Id).ToList();
        }

        private void InitBooks()
        {
            // Arrange
            var book1 = new Book
            {
                Id = Guid.NewGuid(),
                Title = "El señor de los membrillos",
                Author = "J.R.R. Rowling",
                UserId = LoginSvc.CurrentUser.Id
            };

            var book2 = new Book
            {
                Id = Guid.NewGuid(),
                Title = "El planeta de los Nimios",
                Author = "Dr Zhivago",
                UserId = LoginSvc.CurrentUser.Id
            };

            var book3 = new Book
            {
                Id = Guid.NewGuid(),
                Title = "Ulyses",
                Author = "James Joyce",
                UserId = default(Guid)
            };

            BooksSvc.Add(book1);
            BooksSvc.Add(book2);
            BooksSvc.Add(book3);

            // Cuando llamemos al servicio sólo deberíamos ver los dos primeros
            // Act
            Lends = BooksSvc.GetAllByUserId(LoginSvc.CurrentUser.Id).ToList();

            // Assert
            var isCountOk = Lends.Count == 2;
        }

        #region Commands

        public ICommand AddBookCommand { get; set; }

        #endregion
    }
}
