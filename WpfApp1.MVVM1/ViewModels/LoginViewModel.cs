﻿using CrazyBooks.Lib.Services;
using CrazyBooks.Lib.UI;
using CrazyBooks.Lib.UI.Interfaces;
using System.Security.Cryptography;
using System.Windows.Input;

namespace WpfApp1.MVVM1.ViewModels
{
    public class LoginViewModel: ViewModelBase
    {
        public ISwitcher Switcher { get; set; }
        public IPasswordReader PasswordReader { get; set; }

        public ILoginService LoginSvc { get; set; }

        public string UserEmail
        {
            get
            {
                return _userEmail;
            }
            set
            {
                _userEmail = value;
                NotifyPropertyChanged();
            }
        }
        string _userEmail;

        public string UserPassword
        {
            get
            {
                return PasswordReader.GetPassword();
            }
        }

        public string Error
        {
            get
            {
                return _error;
            }
            set
            {
                _error = value;
                NotifyPropertyChanged();
            }
        }
        string _error = string.Empty;

        public LoginViewModel(IPasswordReader passwordReader, ILoginService loginSvc)
        {
            PasswordReader = passwordReader;
            LoginSvc = loginSvc;
            LoginCommand = new RouteCommand(Login);
        }

        public void Login()
        {
            var formatError = string.Empty;

            var isFormatOk = LoginSvc.ValidateFormat(UserEmail, UserPassword, ref formatError);

            if (!isFormatOk)
            {
                // notificar el error
                Error = formatError;
            }
            else
            {
                var loginError = string.Empty;
                var isLoginSuccess = LoginSvc.Login(UserEmail, UserPassword, ref loginError);

                if (!isLoginSuccess)
                {
                    Error = loginError;
                    return;
                }

                if (Switcher != null)
                {
                    Switcher.Switch();
                }
                else
                    Error = "Switcher to home not provided";
            }
        }

        #region Commands
        public ICommand LoginCommand { get; set; }

        #endregion
    }
}
