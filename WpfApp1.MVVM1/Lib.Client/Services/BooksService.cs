﻿using CrazyBooks.Lib.Models;
using CrazyBooks.Lib.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WpfApp1.MVVM1.Lib.Client.Services
{
    public class BooksService : IBooksService
    {
        static Dictionary<Guid, Book> Items
        {
            get
            {
                // forma 1 de toda la vida
                //if (_items == null)
                //{
                //    _items = new Dictionary<Guid, Book>();
                //}
                //return _items;

                // forma 2 para vagos
                //return _items ?? (_items = new Dictionary<Guid, Book>());

                // forma 3 para ultra vagos
                return _items ??= new Dictionary<Guid, Book>();
            }
        }
        static Dictionary<Guid, Book> _items;

        public BooksService()
        {
            
        }

        public bool Add(Book entity)
        {
            if (entity.Id == default)
                entity.Id = Guid.NewGuid();

            if (Items.ContainsKey(entity.Id))
                return false;

            Items.Add(entity.Id, entity);
            return true;
        }

        public IEnumerable<Book> GetAll()
        {
            return Items.Values;
        }

        public IEnumerable<Book> GetAllByUserId(Guid id)
        {
            return GetAll().Where(x => x.UserId == id);
        }
    }
}
