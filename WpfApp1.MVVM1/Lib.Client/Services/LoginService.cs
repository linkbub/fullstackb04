﻿using System;
using CrazyBooks.Lib.Models;
using CrazyBooks.Lib.Services;

namespace WpfApp1.MVVM1.Lib.Client.Services
{
    public class LoginService : ILoginService
    {
        static User _currentUser;
        public User CurrentUser
        {
            get
            {
                return _currentUser;
            }
            set
            {
                _currentUser = value;
            }
        }

        public bool Login(string email, string passowrd, ref string error)
        {
            var formatError = string.Empty;
            var isFormatOk = ValidateFormat(email, passowrd, ref formatError);

            if (!isFormatOk)
            {
                error = formatError;
                return false;
            }

            if (email == "a@a.com" && passowrd == "1234")
            {
                error = string.Empty;

                CurrentUser = new User()
                {
                    Id = Guid.NewGuid(),
                    Email = "a@a.com",
                    Name = "Linkbub",
                    Surname1 = "Academy Superpower!"
                };

                return true;
            }
            else
            {
                error = "User email or password are wrong";
                return false;
            }
        }

        public bool ValidateFormat(string email, string passowrd, ref string error)
        {
            var fieldsAreEmpty = string.IsNullOrEmpty(email) || string.IsNullOrEmpty(passowrd);

            if (fieldsAreEmpty)
                error = "ninguno de los campos puede estar vacío";

            return !fieldsAreEmpty;
        }
    }
}
