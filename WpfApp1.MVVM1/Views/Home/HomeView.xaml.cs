﻿using System.Windows.Controls;
using WpfApp1.MVVM1.Lib.Client.Services;
using WpfApp1.MVVM1.ViewModels;

namespace WpfApp1.MVVM1.Views
{
    /// <summary>
    /// Interaction logic for HomeView.xaml
    /// </summary>
    public partial class HomeView : UserControl
    {
        HomeViewModel HomeVM { get; set; }
        public HomeView()
        {
            InitializeComponent();
        }

        public void Activate()
        {
            HomeVM = new HomeViewModel(new LoginService(), new BooksService());
            DataContext = HomeVM;
        }
    }
}
