﻿using CrazyBooks.Lib.UI.Interfaces;
using System.Windows.Controls;
using WpfApp1.MVVM1.Lib.Client.Services;
using WpfApp1.MVVM1.ViewModels;

namespace WpfApp1.MVVM1.Views
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : UserControl, IPasswordReader
    {
        internal LoginViewModel LoginVM { get; set; } 

        public LoginView()
        {
            InitializeComponent();

            LoginVM = new LoginViewModel(this, new LoginService());
            DataContext = LoginVM;
        }

        public string GetPassword()
        {
            return TbUserPassword.Password;
        }
    }
}
