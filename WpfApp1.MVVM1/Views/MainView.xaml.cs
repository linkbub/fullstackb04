﻿using CrazyBooks.Lib.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp1.MVVM1.ViewModels;

namespace WpfApp1.MVVM1.Views
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : UserControl, ISwitcher
    {
        internal MainViewModel MainVM { get; set; }

        public MainView()
        {
            InitializeComponent();

            MainVM = new MainViewModel();
            this.DataContext = MainVM;

            var loginVM = (LoginViewModel)LoginView.DataContext;
            loginVM.Switcher = this;
        }

        private void BtLoging_Click(object sender, RoutedEventArgs e)
        {
            MainVM.Title = "lo he cambio desde el code behind";
        }

        public void Switch()
        {
            if (LoginView.Visibility == Visibility.Visible)
            {
                LoginView.Visibility = Visibility.Hidden;
                HomeView.Visibility = Visibility.Visible;

                HomeView.Activate();
            }
        }
    }
}
