import { Guid } from '../Tools/Guid.js'

export class BooksService
{
    static #_items;
    static get Items()
    {        
        return BooksService.#_items ?? (BooksService.#_items = new Map());        
    }

    constructor()
    {
        
    }

    Add(entity)
    {
        if (entity.Id === Guid.Default)
            entity.Id = Guid.NewGuid();

        if (BooksService.Items.has(entity.Id))
            return false;

            BooksService.Items.set(entity.Id, entity);
        return true;
    }

    GetAll()
    {
        return Array.from(BooksService.Items.values());
    }

    GetAllByUserId(id)
    {
        return BooksService.Items.Where(x => x.UserId === id);
    }
}

CrazybooksApp.service('BooksService', BooksService);