import { User } from '../Models/User.js'
import { Guid } from '../Tools/Guid.js'

export class LoginService
{
    static #_currentUser;

    get CurrentUser()
    {        
        return LoginService.#_currentUser;
        
    }
    set CurrentUser(value)
    {
        LoginService.#_currentUser = value;
    }

    constructor()
    {

    }

    Login(email, passowrd, error)
    {
        var isFormatOk = this.ValidateFormat(email, passowrd, error);

        if (!isFormatOk)
            return false;

        if (email === "a@a.com" && passowrd === "1234")
        {
            error.Message = string.Empty;

            this.CurrentUser = new User();
            this.CurrentUser.Id = Guid.NewGuid();
            this.CurrentUser.Email = "a@a.com";
            this.CurrentUser.Name = "Linkbub";
            this.CurrentUser.Surname1 = "Academy Superpower!";

            return true;
        }
        else
        {
            error.Message = "User email or password are wrong";
            return false;
        }
    }

    ValidateFormat(email, password, error)
    {
        var fieldsAreEmpty = string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password);

        if (fieldsAreEmpty)
            error.Message = "ninguno de los campos puede estar vacío";

        return !fieldsAreEmpty;
    }
}

CrazybooksApp.service('LoginService', LoginService);