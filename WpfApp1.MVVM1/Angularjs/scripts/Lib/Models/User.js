import { Entity } from './Entity.js'

export class User extends Entity
{
    #_name = "";
    #_surname1 = "";
    #_email = "";
    #_password = "";

    get Name()
    {
        return this.#_name;
    }
    set Name(value)
    {
        this.#_name = value;
    }

    get Surname1()
    {
        return this.#_surname1;
    }
    set Surname1(value)
    {
        this.#_surname1 = value;
    }

    get Email()
    {
        return this.#_email;
    }
    set Email(value)
    {
        this.#_email = value;
    }

    get Password()
    {
        return this.#_password;
    }
    set Password(value)
    {
        this.#_password = value;
    }

    constructor()
    {
        super();
    }
}