import { Entity } from './Entity.js'

export class Book extends Entity
{
    #_title = "";
    #_author = "";
    #_userId = "";
    #_user = null;

    get Title()
    {
        return this.#_title;
    }
    set Title(value)
    {
        this.#_title = value;
    }

    get Author()
    {
        return this.#_author;
    }
    set Author(value)
    {
        this.#_author = value;
    }

    get UserId()
    {
        return this.#_userId;
    }
    set UserId(value)
    {
        this.#_userId = value;
    }

    get User()
    {
        return this.#_user;
    }
    set User(value)
    {
        this.#_user = value;
        this.#_userId = value.Id;
    }

    constructor()
    {
        super();
    }

    ToGridDto()
    {
        return { Title: this.Title, Author: this.Author };
    }
}