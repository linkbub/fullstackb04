Array.prototype.Where = function (expression) 
{
    let output = new Array();
    for (let i = 0; i < this.length; i++) 
    {
        let item = this[i];
        if (expression(item))
            output.push(item);
    }

    return output;
};

Object.defineProperty(Array.prototype, 'Count', 
{
    get() 
    { 
        return this.length; 
    }    
  });


Array.prototype.FirstOrDefault = function (expression) 
{
    for (let i = 0; i < this.length; i++) 
    {
        let item = this[i];
        if (expression(item))
            return item;
    }

    return null;
};

Array.prototype.LastOrDefault = function (expression) 
{
    for (let i = this.length - 1; i >= 0 ; i--) 
    {
        let item = this[i];
        if (expression(item))
            return item;
    }

    return null;
};

Array.prototype.Select = function (action) 
{
    let output = new Array();
    for (let i = 0; i < this.length; i++) 
    {
        output.push(action(this[i]));
    }

    return output;
};

Map.prototype.Where = function (expression) 
{
    let output = new Array();
    let iterator = this.values();

    let result = iterator.next();
    while (!result.done) 
    {
        if (expression(result.value))
            output.push(result.value);
        result = iterator.next();
    }

    return output;
};

class string
{
    static IsNullOrEmpty(value)
    {
        return stringIsNullOrEmpty(value);
    }

    static Empty = "";
}

stringIsNullOrEmpty = function (o) 
{
    return o == null ||  o === "";
};

ObjectIsNullOrUndefined = function(o)
{
    return o === undefined || o === null;
};

class Console
{
    static WriteLine(msg)
    {
        console.log(msg);
    }

    static ReadLine()
    {
        return prompt("escriba algo");
    }
}