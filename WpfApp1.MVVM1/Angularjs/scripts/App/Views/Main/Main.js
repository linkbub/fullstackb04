export class Main
{
    Title = "Bienvenidos a Crazybooks!";
    LoginSvc = null;

    get IsLogon()
    {
        return !ObjectIsNullOrUndefined(this.LoginSvc.CurrentUser);
    }
    
    constructor(loginSvc)
    {
        this.LoginSvc = loginSvc;
    }
}

Main.$inject = ['LoginService'];

CrazybooksApp.
    component('main', {
        templateUrl: './scripts/App/Views/Main/Main.html',
        controller: ('Main', Main),
        controllerAs: 'vm'
    });