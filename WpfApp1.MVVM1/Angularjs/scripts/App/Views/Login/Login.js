export class Login
{
    UserEmail = "a@a.com";
    UserPassword = "1234";

    LoginSvc;

    constructor(loginSvc)
    {
        this.LoginSvc = loginSvc;
    }

    Login()
    {
        //alert(`user name:${this.UserEmail} user password:${this.UserPassword}`);
        var error = {};
        var isFormatOk = this.LoginSvc.ValidateFormat(this.UserEmail, this.UserPassword, error);

        if (!isFormatOk)
        {
            alert(error.Message);
        }
        else
        {
            var isLoginSuccess = this.LoginSvc.Login(this.UserEmail, this.UserPassword, error);

            if (!isLoginSuccess)
            {
                alert(error.Message);
                return;
            }
        }
    }
}

Login.$inject = ['LoginService'];

CrazybooksApp.
    component('login', {
        templateUrl: './scripts/App/Views/Login/Login.html',
        controller: ('Login', Login),
        controllerAs: 'vm'
    });