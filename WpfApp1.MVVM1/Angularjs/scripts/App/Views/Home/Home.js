import { Guid } from '../../../Lib/Tools/Guid.js'
import { Book } from '../../../Lib/Models/Book.js'

export class Home
{
    BookTitle;
    AuthorName;

    get Books()
    {
        return this._books;
    }
    set Books(value)
    {
        this._books = value;
    }
    

    IsLogon = false;

    get CurrentUserName()
    {
        if (ObjectIsNullOrUndefined(this.LoginSvc.CurrentUser))
        {
            return string.Empty;
        } 
        else
        {        
            if (!this.IsLogon)
            {    
                this.IsLogon = true;
                this.InitBooks();
            }

            this.LoginSvc.CurrentUser.Name;
        }
    }

    constructor(loginSvc, booksSvc)
    {
        this.LoginSvc = loginSvc;
        this.BooksSvc = booksSvc;

        this._books = [];
    }

    InitBooks()
    {
        // Arrange
        var book1 = new Book();
        
        book1.Id = Guid.NewGuid();
        book1.Title = "El señor de los membrillos";
        book1.Author = "J.R.R. Rowling";
        book1.UserId = this.LoginSvc.CurrentUser.Id;    

        var book2 = new Book();
        
        book2.Id = Guid.NewGuid();
        book2.Title = "El planeta de los Nimios";
        book2.Author = "Dr Zhivago";
        book2.UserId = this.LoginSvc.CurrentUser.Id;   

        var book3 = new Book();
         
        book3.Id = Guid.NewGuid();
        book3.Title = "Ulyses";
        book3.Author = "James Joyce";
        book3.UserId = Guid.default; 

        this.BooksSvc.Add(book1);
        this.BooksSvc.Add(book2);
        this.BooksSvc.Add(book3);

        // Cuando llamemos al servicio sólo deberíamos ver los dos primeros
        // Act
        let lends = this.BooksSvc.GetAllByUserId(this.LoginSvc.CurrentUser.Id);

        // Assert
        var isCountOk = lends.Count == 2;

        this.ClenAndAddItemsToLends(lends)
    }

    AddBook()
    {
        var book = new Book();
        book.Id = Guid.NewGuid();
        book.Title = this.BookTitle;
        book.Author = this.AuthorName;
        book.UserId = this.LoginSvc.CurrentUser.Id;
        
        this.BooksSvc.Add(book);

        let lends = this.BooksSvc.GetAllByUserId(this.LoginSvc.CurrentUser.Id);
        this.ClenAndAddItemsToLends(lends);
    }

    ClenAndAddItemsToLends(books)
    {
        this.Books.length = 0;

        books.forEach((item)=>
        {
            this.Books.push(item.ToGridDto());
        });
    }
}

Home.$inject = ['LoginService', 'BooksService'];

CrazybooksApp.
    component('home', {
        templateUrl: './scripts/App/Views/Home/Home.html',
        controller: ('Home', Home),
        controllerAs: 'vm'
    });