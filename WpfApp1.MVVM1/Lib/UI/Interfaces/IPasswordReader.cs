﻿namespace CrazyBooks.Lib.UI.Interfaces
{
    public interface IPasswordReader
    {
        string GetPassword();
    }
}
