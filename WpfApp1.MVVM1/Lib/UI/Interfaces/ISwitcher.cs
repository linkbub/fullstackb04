﻿namespace CrazyBooks.Lib.UI
{
    public interface ISwitcher
    {
        void Switch();
    }
}
