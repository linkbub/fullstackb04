﻿using CrazyBooks.Lib.Models;

namespace CrazyBooks.Lib.Services
{
    public interface ILoginService
    {
        User CurrentUser { get; set; }

        bool Login(string email, string passowrd, ref string error);

        bool ValidateFormat(string email, string passowrd, ref string error);
    }
}
