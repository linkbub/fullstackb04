﻿using CrazyBooks.Lib.Models;
using System;
using System.Collections.Generic;

namespace CrazyBooks.Lib.Services
{
    public interface IBooksService
    {
        IEnumerable<Book> GetAll();
        IEnumerable<Book> GetAllByUserId(Guid id);

        bool Add(Book entity);
    }
}
