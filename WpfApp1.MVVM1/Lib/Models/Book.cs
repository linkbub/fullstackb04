﻿using System;

namespace CrazyBooks.Lib.Models
{
    public class Book : Entity
    {
        public Guid UserId { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }
    }
}
