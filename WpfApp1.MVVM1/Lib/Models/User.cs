﻿namespace CrazyBooks.Lib.Models
{
    public class User : Entity
    {
        public string Name { get; set; }
        public string Surname1 { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
