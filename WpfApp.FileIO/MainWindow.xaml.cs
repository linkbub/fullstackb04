﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp.FileIO
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            #region 1 obtenemos un fichero

            var ofd = new OpenFileDialog();
            var result = ofd.ShowDialog();
            var fileName = string.Empty;


            if (result.HasValue && result.Value == true)
            {
                fileName = ofd.FileName;
                TbFileName.Text = fileName;
            }

            #endregion

            #region 2 Lo leemos con StreamReader
            // introducir el concepto de using y dispose
            
            using (var sr1 = new StreamReader(fileName))
            {
                var sb = new StringBuilder();

                string line;
                while ((line = sr1.ReadLine()) != null)
                {
                    sb.AppendLine(line);
                }

                TbFileCode.Text = sb.ToString();

                sr1.Close();
            }
            #endregion

            #region  3 lo modificamos con StreamWriter
            // quiero escribir un fichero con el mismo nombre más la palabra copy
            // conservando la extensión
            // a partir de lo que hayamos metido en la app

            // 3.1 separa nombre y extension
            var fi = new FileInfo(fileName);
            var extn = fi.Extension;
            var justFileName = fi.Name.Replace(extn, string.Empty);
            var path = fi.DirectoryName;

            var newFileName = @$"{path}\{justFileName}_copy{extn}";

            // 3.2 lo pasamos por el stream writer

            using(var sw1 = new StreamWriter(newFileName))
            {
                sw1.Write(TbFileCode.Text);
                sw1.Close();
            }

            #endregion

            #region  4 Lo escribimos con File.Write

            var allText = File.ReadAllText(fileName);
            File.WriteAllText(newFileName, TbFileCode.Text);

            #endregion

            #region Obtenemos la carpeta actual con Directory y creamos una carpeta nueva           

            var directorioActual = Directory.GetCurrentDirectory();

            var info = System.IO.Path.GetDirectoryName(directorioActual);

            if (!Directory.Exists(directorioActual + "\\Temp"))
                Directory.CreateDirectory(directorioActual + "\\Temp");

            #endregion

        }
    }
}
