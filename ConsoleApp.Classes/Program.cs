﻿using System;
using ConsoleApp.Classes.Models;

namespace ConsoleApp.Classes
{
    class Program
    {
        public static int Hour = 5;

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World! " + Hour);

            var employee = new Employee();

            var person = employee as Person;
            var user = employee as User;

            var customer = person as Customer; // esto dará null

            employee.TellTo("hola cómo estás", "pepe");
            user.TellTo("hola cómo estás", "pepe");
            person.TellTo("hola cómo estás", "pepe");

            customer.TellTo("hola cómo estás", "pepe");

            var v1 = new Vector2(5, 2);
            var v2 = new Vector2(1, 8);

            var v3 = v1 + v2;
        }
    }
}
