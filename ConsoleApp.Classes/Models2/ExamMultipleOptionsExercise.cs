﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp.Classes.Models2
{
    public class ExamMultipleOptionsExercise : Exercise
    {

        public Guid ExamId { get; set; }

        public Guid MultipleOptionsExercise { get; set; }
    }
}
