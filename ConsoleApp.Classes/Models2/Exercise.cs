﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp.Classes.Models2
{
    public class Exercise : Entity
    {
        public string Title { get; set; }
        public double FinalMark { get; set; }
        public string ExerciseDescription { get; set; }

    }
}
