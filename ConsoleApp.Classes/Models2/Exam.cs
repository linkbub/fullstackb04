﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace ConsoleApp.Classes.Models2
{
    public class Exam : Entity
    {
       
        public string Title { get; set; }
        public double FinalMark { get; set; }

        public int TotalExercises { get; set; }

        public List<ExamCodeExercise> ExamCodeExercisesList { get; set; }
        public List<ExamMultipleOptionsExercise> ExamMultipleOptionsList { get; set; }
        public List<ExamReplaceWordsExercise> ExamReplaceWordsExerciseList { get; set; }
       


    }
}
