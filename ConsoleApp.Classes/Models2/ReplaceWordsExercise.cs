﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp.Classes.Models2
{
    public class ReplaceWordsExercise : Exercise
    {
        public List<Exam> ExamList { get; set; }
        public string FinalDescription { get; set; }

        public string WordToReplace { get; set; }
        public string TeacherResult { get; set; }
    }
}
