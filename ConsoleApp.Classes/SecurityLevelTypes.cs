﻿namespace ConsoleApp.Classes
{
    public enum SecurityLevelTypes
    {
        None, 
        Basic,
        SysAdmin,
        Customer,
        Worker
    }
}
