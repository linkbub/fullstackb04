﻿using System;

namespace ConsoleApp.Classes.Models
{
    public abstract class Person
    {
        public string Dni
        {
            get
            {
                return dni;
            }
            set
            {
                dni = value;
            }
        }
        private string dni = ""; 


        public string Name { get; set; }

        public string Surname { get; set; }

        protected int StartYear { get; set; }

        public Person()
        {

        }

        public Person(string dni)
        {

        }

        public virtual void TellTo(string message, string to)
        {
            Console.WriteLine($"hola {to} te quería decir que: {message}");
        }     
        
    }
}
