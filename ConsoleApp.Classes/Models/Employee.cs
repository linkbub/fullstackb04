﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp.Classes.Models
{
    public sealed class Employee : User
    {
        public Employee()
        {
            this.StartYear = 4;
        }

        public new void TellTo(string message, string to)
        {
            base.TellTo(message, to);
            Console.WriteLine($"Bienvenido a nuestro local, póngase a gusto");
        }
    }
}
