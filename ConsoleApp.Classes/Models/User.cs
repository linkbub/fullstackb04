﻿using System;

namespace ConsoleApp.Classes.Models
{
    public abstract class User : Person 
    {        
        public SecurityLevelTypes SecurityLevelType { get; set; }

        public User() 
            : base()
        {

        }

        public User(string dni)
            : base(dni)
        {

        }

        public new void TellTo(string message, string to)
        {
            base.TellTo(message, to);
            LogMessage("log de escritura del sistema " + message, SecurityLevelType);
        }

        public virtual void LogMessage(string msg, SecurityLevelTypes authLevel)
        {
            // do somethoi
        }

    }
}
