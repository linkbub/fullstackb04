﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    public class Client : Persona
    {
        public static int EdadMaxima = 100;

        public int EdadActual
        {
            get
            {
                return _edadActual;
            }
            set
            {
                if (value > EdadMaxima)
                    throw new ArgumentException("la edad supera a la edad máxima");

                _edadActual = value;
            }
        }

        private int _edadActual = 0;

        public int MesaActual { get; set; }

        public Client(string name, int year = 2020)
            : base(name, year)
        {
            this.Order(1, "aceitunas", false);
        }

        public override void Sentarse()
        {
            base.Sentarse();
            Console.WriteLine("me siento en una mesa");
        }

        public void Order(int waiterId, string plato, bool tengoPrisa = false)
        {
            Console.WriteLine($"el cliente {Name} le pide al camarerorrrr {waiterId} un plato de {plato}");
        }
    }
}
