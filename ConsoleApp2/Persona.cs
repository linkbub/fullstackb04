﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    public class Persona
    {
        private string _name;

        public string Name
        {
            get
            {
                return _name;
            }
            private set
            {
                if (value.Length == 0)
                {
                    throw new ArgumentException("el valor de nombre no puede estar vacío");
                }
                else if (Char.IsLower(value[0]))
                {
                    _name = value[0].ToString().ToUpper() + value.Substring(1, value.Length - 1);
                }
                else
                {
                    _name = value;
                }
            }
        }

        public int Year { get; set; }

        public Persona()
        {

        }

        public Persona(string name, int year)
        {

        }

        public Persona(string name)
        {
            Name = name;
        }

        public virtual void Sentarse()
        {
            Console.WriteLine("Lo primero es buscar un sitio donde sentarme");
        }
    }
}
