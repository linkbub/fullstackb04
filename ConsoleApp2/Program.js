console.log("Hello pipol!");

function GetAverageExamRank(grades)
{
    // usando bucle while
    let count1 = 0;
    let suma1 = 0.0;

    while (true) {
        if (grades[count1] < 0)
            continue;

        //suma = suma + grades[count];
        suma1 += grades[count1];
        count1++;

        if (count1 == grades.length)
            break;
    }

    let avg1 = suma1 / grades.length;

    // usando bucle for

    let suma2 = 0.0;

    for (let i = 0; i < grades.length; i++) {
        suma2 += grades[i];
    }

    let avg2 = suma2 / grades.length;

    // usando el foreach

    let suma3 = 0.0;

    // foreach (let grade in grades)
    grades.forEach((grade) => {
        suma3 += grade;
    });

    let avg3 = suma2 / grades.Count;

    return avg3;

}