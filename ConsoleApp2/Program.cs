﻿using System;
using System.Collections.Generic;

namespace ConsoleApp2
{
    class Program
    {
        static int CurrentYear = 2020;

        static void Main(string[] args)
        {
            Console.WriteLine("Hola Pipol!");
            
            var num1 = 8.5;
            var num2 = 9.0;

            PrintAverage(num1, num2);

            var num3 = 9.4;
            var num4 = 3.4;

            PrintAverage(num3, num4);


            var edadMáxima = Client.EdadMaxima;

            var client1 = new Client("Pepe");
            client1.EdadActual = 30;
            client1.EdadActual = 102;

            Console.WriteLine("hola " + client1.Name);

            //client1.Name = "Pepe";
            client1.Order(1, "callos a la madrileña", true);            
        }

        static void SayHello()
        {
            Console.WriteLine("hola");
        }

        static double GetAverage(double a, double b)
        {
            var output = (a + b) / 2;
            return output;
        }

        static void PrintAverage(double a, double b)
        {
            var avg = GetAverage(a, b);
            var msg = "La media de " + a.ToString() + " y " + b + " es " + avg;
            Console.WriteLine(msg);
            Console.WriteLine($"La media de {a} y {b} es {avg}");
        }    
        
    }
}
