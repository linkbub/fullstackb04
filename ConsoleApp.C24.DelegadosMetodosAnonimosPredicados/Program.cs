﻿using ConsoleApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http.Headers;

namespace ConsoleApp.C24.DelegadosMetodosAnonimosPredicados
{
    class Program
    {
        delegate void TestDelegate(string s);

        static void Main(string[] args) 
        { 

            Console.WriteLine("Hello World!");

            #region delegados y asignación

            // Forma 1 apuntando a un método de clase estático

            //PerformOperation opSum1 = Suma; esto también es válido
            var opSum1 = new PerformOperation(Suma);

            ///// Forma 2 Usando funciones anónimas
            //Forma 2.1asignando un método anónimo
            PerformOperation opSum2_1 = delegate (int a, int b) { return a + b; };

            // Forma 2.2 asignando una expresión lambda
            PerformOperation opSum2_2_1 = (a, b) => a + b;
            var opSum2_2_2 = new PerformOperation((a, b) => a + b);

            ////////////////////////////////////////


            var suma1 = opSum1(4, 6);
            var suma2_1 = opSum2_1(4, 6);
            var suma2_2_1 = opSum2_2_1(4, 6);
            var suma2_2_2 = opSum2_2_2(4, 6);

            var opSumFunc = new Func<int, int, double>(Suma);
            var sumaFunc = opSumFunc(4, 6);


            #endregion

            #region Predicate

            var p = new Predicate<Entity>(x => x.Id != default);

            Func<Entity, bool> f = x => x.Id != default;

            Expression<Func<Entity, bool>> ef = x => x.Id != default;
            //var expression = Expression.Lambda<Func<Entity, bool>>(Expression.Call(f.Method));

            var list = new List<User>();
            var a = list.Find(p);
            var b = list.Where(x => x.Id != default).Where(x => x.Name.StartsWith("J"));
            var c = list.AsQueryable().Where(x => x.Id != default).Where(x => x.Name.StartsWith("J")).ToList();

            #endregion

            //https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/language-specification/expressions#anonymous-function-expressions
        }

        public static double Calculate(CalculationOptions options, int num1, int num2)
        {
            PerformOperation calc;

            switch (options)
            {
                case CalculationOptions.Add:
                    calc = Suma;
                    break;
                case CalculationOptions.Sub:
                    calc = Restar;
                    break;
                case CalculationOptions.Multipl:
                    calc = Multiply;
                    break;
                default:
                case CalculationOptions.Div:
                    calc = Dividir;
                    break;
            }

            return ResolveCalculation(calc, num1, num2);
        }

        public static double ResolveCalculation(PerformOperation calc, int num1, int num2)
        {
            return calc(num1, num2);
        }

        public static double Suma(int num1, int num2)
        {
            return num1 + num2;
        }

        public static double Restar(int num1, int num2)
        {
            return num1 - num2;
        }

        public static double Multiply(int num1, int num2)
        {
            return num1 * num2;
        }

        public static double Dividir(int num1, int num2)
        {
            return num1 / num2;
        }
    }

    public delegate double PerformOperation(int a, int b);

    public enum CalculationOptions
    {
        Add = 1,
        Sub,
        Multipl,
        Div
    }
}
