﻿using System;

namespace ConsoleApp.Structs
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Employee emp;
            emp.Salary = 20000.0;

            Console.WriteLine($"{emp.Salary}");

            var point = new Point(4, 6);
        }
    }

    class Client
    {
        public double Salary
        {
            get
            {
                return _salary;
            }
            set
            {
                if(_salary <= 0)
                {
                    throw new ArgumentException("eso es esclavitud");
                }
                _salary = value;
            }
        }

        private double _salary;

        public Client()
        {

        }
    }

    struct Point
    {
        public int x;
        public int y;

        public Point(int _x, int _y)
        {
            x = _x;
            y = _y;
        }

        public void Print()
        {
            Console.WriteLine($"{x},{y}");
        }
    }

    struct Employee
    {
        public double Salary;

        static Employee()
        {
            Console.Write("First object created");
        }

        public Employee(double salary)
        {
            Salary = salary; 
        }
    }
}
