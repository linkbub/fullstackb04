﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp.C26.Utilidades
{
    class Examples_Tuples
    {
        IGoogleMapLocationService LocationService { get; set; }

        public Examples_Tuples(List<Student> students)
        {
            var data = new List<Tuple<Guid, string, string, string>>();

            foreach(var s in students)
            {
                data.Add(new Tuple<Guid, string, string, string>(s.Id, s.Name, s.Email, LocationService.GetCity(s.Lat, s.Lng)));
            }

            ReportUsersData(data);
        }

        /// <summary>
        /// Un método que recibe datos agrupados para operar con ellos
        /// Nos interesa mantener estos métodos en privado ya que estamos dando pie
        /// a posibles fallos en los datos ya que la única comprobación que estamos haciendo
        /// es el tipo, pero se pueden cruzar
        /// </summary>
        /// <param name="data"></param>
        void ReportUsersData(List<Tuple<Guid, string, string, string>> data)
        {
            foreach (var t in data)
                Console.WriteLine($"id:{t.Item1} name:{t.Item2} email:{t.Item3} city:{t.Item4}");
        }

    }

    class Student
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string Email { get; set; }

        public double Lat { get; set; }
        public double Lng { get; set; }
    }

    public interface IGoogleMapLocationService
    {
        string GetCity(double lat, double lng);
    }
}
