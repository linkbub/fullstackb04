﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleApp.C26.Utilidades
{
    class Examples_StringBuilder
    {
        IGoogleMapLocationService LocationService { get; set; } = new LocationService();

        public Examples_StringBuilder(string jsonPath)
        {
            // leemos con el file y sacamos el json en string
            var json = File.ReadAllText(jsonPath);
            
            // lo deserializamos a List<student>
            var students = JsonConvert.DeserializeObject<List<Student>>(json);

            // y ya lo podemos usar
            ProcessInfo(students);
        }

        public Examples_StringBuilder(List<Student> students)
        {
            ProcessInfo(students);
        }

        public void ProcessInfo(List<Student> students)
        {
            //var output = string.Empty;
            var sb = new StringBuilder();

            foreach (var s in students)
            {
                // wrong
                //output += $"id:{s.Name} name:{s.Name} email:{s.Name} city:{LocationService.GetCity(s.Lat, s.Lng)}\n";

                // right
                sb.AppendLine($"id:{s.Name} name:{s.Name} email:{s.Name} city:{LocationService.GetCity(s.Lat, s.Lng)}");
            }

            WriteToFileTxt(sb.ToString());

            SerializeToJson(students);
        }

        private void SerializeToJson(List<Student> students)
        {
            var json = JsonConvert.SerializeObject(students);
            File.WriteAllText(@"C:\temp\output.json", json);
        }

        public void WriteToFileTxt(string text)
        {
            // opción usando streams
            //using (var sb1 = new StreamWriter(@"C:\temp\output.txt"))
            //{
            //    sb1.Write(text);
            //    sb1.Close();
            //}

            // escribir
            File.WriteAllText(@"C:\temp\output.txt", text);
        }
    }

    public class LocationService : IGoogleMapLocationService
    {
        public string GetCity(double lat, double lng)
        {
            return "Barcelona";
        }
    }
}
