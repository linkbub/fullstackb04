﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ConsoleApp.C26.Utilidades
{
    class Example_WebClient
    {
        public Example_WebClient()
        {
            var wc = new WebClient();
            var myJsonResponse = wc.DownloadString("https://www.bitstamp.net/api/ticker/");

            var info = JsonConvert.DeserializeObject<BitstampTickerInfo>(myJsonResponse);

            Console.WriteLine($"volume:{info.volume} last price:{info.last}");
        }
    }

    public class BitstampTickerInfo
    {
        public string volume { get; set; }
        public string last { get; set; }
        public string timestamp { get; set; }
        public string bid { get; set; }
        public string vwap { get; set; }
        public string high { get; set; }
        public string low { get; set; }
        public string ask { get; set; }
        public double open { get; set; }
    }


}
