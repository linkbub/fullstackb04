﻿using System;
using System.Collections.Generic;

namespace ConsoleApp.C26.Utilidades
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            // Ejemplo StringBuild, File & Json
            //var students = new List<Student>();

            //students.Add(new Student() { Id = Guid.NewGuid(), Email = "a@a.com", Name = "Jose", Lat = 0, Lng = 0 });
            //students.Add(new Student() { Id = Guid.NewGuid(), Email = "a@a1.com", Name = "Jose1", Lat = 10, Lng = 0 });
            //students.Add(new Student() { Id = Guid.NewGuid(), Email = "a@a2.com", Name = "Jose2", Lat = 20, Lng = 0 });
            //students.Add(new Student() { Id = Guid.NewGuid(), Email = "a@a3.com", Name = "Jose3", Lat = 30, Lng = 0 });
            //students.Add(new Student() { Id = Guid.NewGuid(), Email = "a@a4.com", Name = "Jose4", Lat = 40, Lng = 0 });
            //students.Add(new Student() { Id = Guid.NewGuid(), Email = "a@a5.com", Name = "Jose5", Lat = 50, Lng = 0 });


            ////var sbe = new Examples_StringBuilder(students);
            //var sbe = new Examples_StringBuilder(@"C:\temp\output.json");

            //Ejemplo WebClient
            var wce = new Example_WebClient();
        }
    }
}
