﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleApp.C23.FuncAction.Models;

namespace ConsoleApp.C23.FuncAction
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("B04.C23 Func y Actions");

            Action<string> showMessageAction = null;
            var userName = Console.ReadLine();

            if(userName.StartsWith("profesor"))
            {
                showMessageAction = new Action<string>(ShowMessageToTeacherInConsole);
                showMessageAction = ShowMessageToTeacherInConsole;

                userName = userName.Replace("profesor", string.Empty);
            }
            else
            {
                showMessageAction = new Action<string>(ShowMessageToStudentInConsole);
            }

            HandleRegister.ShowGenericMessageAction = ShowMessage;

            // esto sería usando un método anónimo
            //HandleRegister.ShowGenericMessageAction = (s) => Console.WriteLine(s);

            Func<string, bool> veryFormatAcion = new Func<string, bool>(VerifyFormat);

            // esto sería usando un método anónimo
            //veryFormatAcion = (s) => !s.Any(char.IsDigit);

            if (HandleRegister.VerifyUser(VerifyFormat, userName))
                HandleRegister.HandleNewUser(showMessageAction, userName);


            var list = new List<User>
            {
                new User()
                {
                    Id = Guid.NewGuid()
                },
                new User()
                {
                    Id = Guid.NewGuid()
                },
                new User()
                {
                    Id = Guid.NewGuid()
                }
            };

            var updateTimeAction = new Action<Entity>(UpdateTime);

            // forma 1: pasando la variable
            list.DoForeach(updateTimeAction);

            // forma 2: pasando el método para que el compilador lo entienda y cree el Action por detrás
            list.DoForeach(UpdateTime);

            // forma 3: pasando una expresión lambda que se transformaría en un método anónimo (no en un Expression Tree porque no es verdadero/falso)
            list.DoForeach(u => u.UpdatedOn = DateTime.Now);

            // forma 4: pasando un método anónimo
            list.DoForeach((u) =>
            {
                u.UpdatedOn = DateTime.Now;
            });
        }

        public static void UpdateTime(Entity entity)
        {
            entity.UpdatedOn = DateTime.Now;
        }

        public static void ShowMessage(string msg)
        {
            Console.WriteLine(msg);
        }

        public static void ShowMessageToStudentInConsole(string name)
        {
            Console.WriteLine($"hello student {name}, be ready to start the class");
        }

        public static void ShowMessageToTeacherInConsole(string name)
        {
            Console.WriteLine($"hello profesor {name}, welcome to your class");
        }

        public static bool VerifyFormat(string strToCheck)
        {

            return !strToCheck.Any(char.IsDigit);
        }
    }

    static class EnumerableExtensions
    {
        public static void DoForeach<T>(this IEnumerable<T> collection, Action<T> modifyEntityAction) where T : Entity
        {
            foreach(var entity in collection)
            {
                modifyEntityAction(entity);
            }
        }
    }
}
