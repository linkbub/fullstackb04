﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp.C23.FuncAction.Models
{
    public class Entity
    {
        public Guid Id { get; set; }

        public DateTime UpdatedOn { get; set; }
    }
}
