﻿using System;

namespace ConsoleApp.C23.FuncAction
{
    public static class HandleRegister
    {
        public static Action<string> ShowGenericMessageAction { get; set; }

        public static void HandleNewUser(Action<string> showMessageAct, string user)
        {
            showMessageAct(user.Trim().ToUpper());
        }

        public static bool VerifyUser(Func<string, bool> verificationAction, string user)
        {
            var result = verificationAction(user);

            if(ShowGenericMessageAction != null)
            {
                if (result)
                    ShowGenericMessageAction("user verification is ok");
                else
                    ShowGenericMessageAction("user verification failed");
            }

            return result;
        }

    }
}
