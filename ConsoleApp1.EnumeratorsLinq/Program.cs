﻿using ConsoleApp.Generics.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp1.EnumeratorsLinq
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            #region 1 Creación de un Enumerador
            /*
            var miEnumerable = new GenericEnumerable<string>();
            var currentEnumerator = miEnumerable.GetEnumerator();

            while (currentEnumerator.MoveNext())
            {
                Console.WriteLine(currentEnumerator.Current.ToString());
            }

            foreach(var element in miEnumerable)
            {

            }*/
            #endregion

            #region 2 Collections Initializers

            var lista = new List<string> { "pepe", "lolo" };

            var dict = new Dictionary<int, string>
            {
                { 1, "pepe" },
                { 5, "lolo" }
            };

            var dict2 = new Dictionary<int, string>
            {
                [1] = "pepe",
                [5] = "lolo"
            };


            #endregion

            #region 3 Iteradores

            foreach (var fib in Fibonaci(8))
            {
                Console.Write($"{fib} ");
                if (fib == 5)
                    break;
            }
            Console.WriteLine("");
            
            foreach (var fib in CalcFibonaci(8))
            {
                if (fib == 5)
                    break;
                Console.Write($"{fib} ");
            }


            #endregion

            #region Muy breve Introducción a Lambda

            var years = new List<int> { 1, 2, 8, -4, 5, 6 };

            //var existeElAño5 = years.Any(CheckYearIs5);
            var existeElAño5 = years.Any(x => x == 5);

            var existeElAño9 = false;
            foreach(var year in years)
            {
                if (year == 9)
                {
                    existeElAño9 = true;
                    break;
                }
            }

            Console.WriteLine(existeElAño9);

            #endregion

            #region Instrucciones típicas en LINQ

            var students = new List<Student>();

            // ejemplo del Where (que equivale al Select de SQL)


            // forma sintaxis linq
            var studentsInNET2 = (from st in students
                                 where st.ClassCode == "NET"
                                 select st).ToList();

            // forma fluent
            var studentsInNET = students.Where(x => x.ClassCode == "NET");


            //ejemplo del First, FirstOrDefault

            var name = "Pepe";
            var pepe1 = students.First(x => x.Name == name);
            var pepe2 = students.FirstOrDefault(x => x.Name == name && x.ClassCode == "NET");


            //ejemplo del Last, LastOrDefault

            var pepe3 = students.Last(x => x.Name == name);
            var pepe4 = students.LastOrDefault(x => x.Name == name && x.ClassCode == "NET");

            // ejemplo del Any
            var existeUnStudentQueSeLlamePepe = students.Any(x => x.Name == name);

            // ejemplo del Select
            var grades = students.Select(x => x.Grade);
            PrintAverage(grades);
            var notas = students.Select(x => x.Grade).ToList();

            var specialMentions = students
                                    .Where(x => x.Grade > 8)
                                    .Select(x => new SpecialMention()
                                    {
                                        StudentName = x.Name,
                                        Grade = x.Grade
                                    });

            // ejemplo OrderBy, OrderByDescending
            var mentionsByName = specialMentions.OrderBy(x => x.StudentName);
            var mentionsByNameInverse = mentionsByName.OrderByDescending(x => x.StudentName);

            // ejemplo Take, SKip

            var pageSize = 20;
            var currentPage = 0;

            while(true)
            {
                var mentionsSet = mentionsByName.Take(pageSize).Skip(currentPage * 20);

                // enviar el paque de datos al cliente
                foreach (var mention in mentionsSet)
                    Console.WriteLine($"Página {currentPage} Student:{mention.StudentName} Grade: {mention.Grade}");

                if (mentionsSet.Count() < pageSize)
                    break;

                currentPage++;

            }

            #endregion
        }

        static void PrintAverage(IEnumerable<double> grades)
        {
            if (grades.Count() > 5)
                Console.WriteLine(grades.Sum());
            else
            {
                PrintHonors(grades.Where(x => x > 8));
            }
        }

        static void PrintHonors(IEnumerable<double> grades)
        {
            Console.WriteLine(grades.Max());
        }

        static bool CheckYearIs5(int x)
        {
            return x == 5;
        }

        static IEnumerable<int> CalcFibonaci(int fibCount)
        {
            var output = new List<int>();

            int prev = -1;
            int next = 1;

            for (var i = 0; i < fibCount + 1; i++)
            {
                var sum = prev + next;
                prev = next;
                next = sum;
                
                if(sum != 0)
                    output.Add(sum);
            }

            return output;
        }

        static IEnumerable<int> Fibonaci(int fibCount)
        {
            for (int i = 0, prevFib = 1, curFib = 1; i < fibCount; i++)
            {
                yield return prevFib;
                int newFib = prevFib + curFib;
                prevFib = curFib;
                curFib = newFib;
            }
        }
    }

    class SpecialMention
    {
        public string StudentName { get; set; }
        public double Grade { get; set; }
    }

    class MyEnumerator : IEnumerator, IDisposable
    {
        public object Current { get; set; }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public bool MoveNext()
        {
            throw new NotImplementedException();
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }
    }

    class GenericEnumerable<T> : IEnumerable<T>
    {
        //protected MyEnumerator MyEnumerator = new MyEnumerator();
        T[] Items = new T[1000];

        public IEnumerator GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return Items.GetEnumerator() as IEnumerator<T>;
        }

        public void Add(T element)
        {
        
        }
    }

    class 
}
