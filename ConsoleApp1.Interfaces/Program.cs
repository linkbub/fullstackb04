﻿using ConsoleApp1.Interfaces.Models;
using System;
using System.Collections.Generic;

namespace ConsoleApp1.Interfaces
{
    class Program
    {
        public static TrafficLight CurrentTrafficLight { get; set; }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            CurrentTrafficLight = new TrafficLight();

            var car = new Car()
            {
                LicenseId = "4142DNP"
            };

            var moto = new Moto()
            {
                LicenseId = "1234a"
            };

            CurrentTrafficLight.AddVehicle(car);
            CurrentTrafficLight.AddVehicle(moto);


            CurrentTrafficLight.StartTrafic();

        }

    }
}
