﻿using System;

namespace ConsoleApp1.Interfaces.Models
{
    public class Car : IVehicle, ITaxable
    {
        public string LicenseId { get; set; }
        public double NormalSpeed { get; set; } = 30;

        public double DetermineCurrentMarketValue()
        {
            return 10000.0;
        }

        public bool MoveForward()
        {
            Console.WriteLine($"el coche se mueve a {NormalSpeed}");
            return true;
        }

        public bool StartEngine()
        {
            Console.WriteLine("el coche arranca");
            return true;
        }
    }
}
