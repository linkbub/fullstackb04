﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1.Interfaces.Models
{
    public class TrafficLight
    {
        public Dictionary<string, IVehicle> CurrentVehicles { get; private set; } = new Dictionary<string, IVehicle>();

        public bool IsMoving { get; set; }

        public TrafficLight()
        {

        }

        public void StartTrafic()
        {
            foreach (var vehicle in CurrentVehicles.Values)
            {
                vehicle.StartEngine();
                vehicle.MoveForward();
            }
        }

        public void AddVehicle(IVehicle vehicle)
        {
            if (IsMoving)
                throw new InvalidOperationException("no se puede añadir un vehículo a la circulación mientras está el semáforo en rojo");

            if (CurrentVehicles.ContainsKey(vehicle.LicenseId))
                throw new ArgumentException("el vehículo ya en la carretera");

            CurrentVehicles.Add(vehicle.LicenseId, vehicle);
        }
    }
}