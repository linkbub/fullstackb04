﻿using System;

namespace ConsoleApp1.Interfaces.Models
{
    public class Moto : IVehicle, ITaxable
    {
        public string LicenseId { get; set; }
        public double NormalSpeed { get; set; } = 20;

        public double DetermineCurrentMarketValue()
        {
            throw new NotImplementedException();
        }

        public bool MoveForward()
        {
            throw new NotImplementedException();
        }

        public bool StartEngine()
        {
            throw new NotImplementedException();
        }
    }
}
