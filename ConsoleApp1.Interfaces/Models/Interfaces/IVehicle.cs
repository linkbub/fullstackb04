﻿namespace ConsoleApp1.Interfaces.Models
{
    public interface IVehicle : IPurchasable
    {
        string LicenseId { get; set; }
        double NormalSpeed { get; set; }

        bool StartEngine();
        bool MoveForward();
    }
}
