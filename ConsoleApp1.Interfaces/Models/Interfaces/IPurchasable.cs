﻿namespace ConsoleApp1.Interfaces.Models
{
    public interface IPurchasable
    {
        double DetermineCurrentMarketValue();
    }
}
