﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Lib.ExtensionMethods;

namespace ConsoleApp.ExtensionMethods
{
    class Program
    {
        static GeoLocalization Tracker { get; set; }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var lista = new List<int>();

            var elements = lista.Where(x => x > 5);

            var elementos = lista.Donde(x => x > 5);

            double day = 40000000.0;

            var dayAsDate = day.ToDateTime();


            var s = Convert.FromBase64String("hola");
        }

        public static void AssignVector(Vector2 v2)
        {
            Tracker.Position = v2.ToCoordinates();
        }

    }

    public static class Vector2Extensions
    {
        public static Coordinates ToCoordinates(this Vector2 v)
        {
            return new Coordinates()
            {
                Lat = v.X,
                Lng = v.Y
            };
        }
    }

    public class GeoLocalization
    {
        public Coordinates Position { get; set; }
    }

    public class Coordinates
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
    }

    public struct Vector2
    {
        public float X { get; set; }
        public float Y { get; set; }
    }
}
