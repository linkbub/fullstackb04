﻿using System;
using System.Collections.Generic;

namespace Common.Lib.ExtensionMethods
{
    static class MyConversionsExtensions
    {
        public static DateTime ToDateTime(this double d)
        {
            return DateTime.FromOADate(d);
        }
    }

}
