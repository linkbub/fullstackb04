﻿using System;
using System.Collections.Generic;

namespace Common.Lib.ExtensionMethods
{
    static class MyEnumerableExtensions
    {
        public static IEnumerable<T> Donde<T>(this IEnumerable<T> o, Func<T, bool> expresion)
        {
            var output = new List<T>();

            foreach(var item in o)
            {
                if (expresion(item))
                    output.Add(item);
            }

            return output;
        }
    }

}
