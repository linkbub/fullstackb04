﻿using ConsoleApp.Generics.Collections;
using ConsoleApp.Generics.Models;

namespace ConsoleApp.Generics.DAL
{
    public interface IUsersList<Tuser> : IEntityList<Tuser> where Tuser : User
    {
    }
}
