﻿using ConsoleApp.Generics.Models;

namespace ConsoleApp.Generics.Collections
{
    public interface IEntityList<TEntity> : IGenericList<TEntity> where TEntity : Entity, new()
    {

    }
}
