﻿namespace ConsoleApp.Generics.Collections
{
    public interface IGenericList<T> : IGenericList
    {
        void Add(T item);
    }

    public interface IGenericList
    {
        int Count { get; set; }
        object GetNext();
    }
}
