﻿namespace ConsoleApp.Generics.Collections
{
    public class GenericList : IGenericList
    {
        protected object[] _items;

        protected int _currentIndex = 0;
        public int Count { get; set; }

        public GenericList()
        {
            _items = new object[4];
        }

        protected void AddObject(object item)
        {
            if (Count >= _items.Length)
            {
                // si esto pasa tenemos que redimensionar el array
                var newArray = new object[_items.Length + 4];

                for (var i = 0; i < _items.Length; i++)
                {
                    newArray[i] = _items[i];
                }

                _items = newArray;

            }

            _items[Count] = item;
            Count++;
        }

        public object GetNext()
        {
            var output = _items[_currentIndex];

            if (_currentIndex++ == _items.Length)
                _currentIndex = 0;

            return output;
        }
    }

    public class GenericList<T> : GenericList, IGenericList<T>
    {        
        public T[] Items
        {
            get
            {
                var output = new T[Count];

                for (var i = 0; i < Count; i++)
                {
                    output[i] = (T)_items[i];
                }

                return output;
            }
        }

        public virtual void Add(T item)
        {
            base.AddObject(item);
        }
        
    }
}
