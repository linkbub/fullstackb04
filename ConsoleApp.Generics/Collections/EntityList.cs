﻿using ConsoleApp.Generics.Models;

namespace ConsoleApp.Generics.Collections
{
    public class EntityList<T> : GenericList<T>, IEntityList<T> where T : Entity
    {
        public override void Add(T item)
        {
            // por ejemplo comprobar permisos de usuario o validez de la intenty etc

            base.Add(item);
        }
    }
}
