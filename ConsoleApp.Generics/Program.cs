﻿using ConsoleApp.Generics.Collections;
using ConsoleApp.Generics.DAL;
using ConsoleApp.Generics.Models;
using System;
using System.Collections.Generic;

namespace ConsoleApp.Generics
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            //1 creamos una generic list y la usamos           
            
            IGenericList<int> genericListInt = new GenericList<int>();

            genericListInt.Add(1);
            genericListInt.Add(4);
            genericListInt.Add(8);
            genericListInt.Add(5);
            genericListInt.Add(9);

            var size = genericListInt.Count;
            var genericListString = new GenericList<string>();

            //2 luego la adaptamos a Interface generic
            IGenericList<double> genericListDouble = new GenericList<double>();
            genericListDouble.Add(5);

            var listasGenericas = new GenericList<IGenericList>();
            listasGenericas.Add(genericListInt);
            listasGenericas.Add(genericListString);
            listasGenericas.Add(genericListDouble);

            PrintNextElement(listasGenericas);

            //3 le añadimos una restricción de tipo y creamos un repository

            var repoEntities = new EntityList<Entity>();
            var repoUsers = new UsersList<User>();

            var student = new Student();
            repoUsers.Add(student);

            var subject = new Subject();
            //repoUsers.Add(subject); esto no se puede

            repoEntities.Add(student);
            repoEntities.Add(subject);

            //4 le damos una property generic
            var primerItem = repoEntities.Items[0];

            //5 enseñamos un método genérico
            var compañero = new Student();
            student.SayHello(compañero);
        }

        public static void PrintNextElement(IGenericList listas)
        {
            for (var i = 0; i < listas.Count; i++)
            {
                var lista = (IGenericList)listas.GetNext();
                var element = lista.GetNext();

                Console.WriteLine(element.ToString());
            }
        }

        public static void Hola()
        {
            var lista = new List<User>();
        }
    }
}
