﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp.Generics.Models
{
    public class Student : User
    {
        public string ClassCode { get; set; }
        public double Grade { get; set; }
    }
}
