class CustomEventTarget
{
    listeners = new Map();   

    addEventListener(type, callback, subscriber) 
    {        
        if (!this.listeners.has(subscriber))
            this.listeners.set(subscriber, {})

        if (!(type in this.listeners.get(subscriber))) 
        {
            this.listeners.get(subscriber)[type] = [];
        }
        this.listeners.get(subscriber)[type].push(callback);
    }
  
    removeEventListener(type, callback) 
    {   
        this.listeners.forEach((value, key, map)=>
        {
            if (!(type in value)) 
                return;
        
            let stack = value[type];
            for (var i = 0, l = stack.length; i < l; i++) 
            {
                if (stack[i] === callback)
                {
                    stack.splice(i, 1);
                    return;
                }
            }
        });
    }
  
    dispatchEvent(event) 
    {
        this.listeners.forEach((value, key, map)=>
        {
            event.subscriber = key;

            if (!(event.type in value)) 
                return true;

            let stack = value[event.type].slice();
      
            for (var i = 0, l = stack.length; i < l; i++) 
                stack[i].call(this, event);

            return !event.defaultPrevented;
        });
    }
}