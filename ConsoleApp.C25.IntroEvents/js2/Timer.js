//
// Summary:
//     Provides data for the System.Timers.Timer.Elapsed event.
class ElapsedEventArgs
{
    //
    // Summary:
    //     Gets the date/time when the System.Timers.Timer.Elapsed event was raised.
    //
    // Returns:
    //     The time the System.Timers.Timer.Elapsed event was raised.
    SignalTime = new Date(Date.now());
}

class Timer extends CustomEventTarget 
{
    Interval = 0;
    
    #_internalTimer;   
    #_enable = false;
    get Enable()
    {
        return this._enable;
    }
    set Enable(value)
    {
        this._enable = value;

        if (this._enable && this.Interval > 0)
        {
            this.#_internalTimer = window.setInterval(()=>
            {
                let elapsed = new Event('Elapsed');
                elapsed.sender = this;
                elapsed.e = new ElapsedEventArgs();                
                this.dispatchEvent(elapsed);

            }, this.Interval);
        }
        else
        {
            window.clearInterval(this.#_internalTimer);
        }
    }

    constructor()
    {
        super();
    }
}