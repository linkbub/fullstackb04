Console = {};
Console.WriteLine = (s) => { console.log(s); };

class Program
{
    aTimer = new Timer();

    Main()
    {
        this.aTimer.Interval = 5000;
        this.aTimer.Enable = true;

        this.aTimer.addEventListener('Elapsed', this.OnTimedEvent, this);
        //aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);

        this.aTimer.addEventListener('Elapsed', this.ExecuteVerification, this);
    }

    ExecuteVerification(event)
    {
        let self = event.subscriber;
        self.aTimer.removeEventListener('Elapsed', self.ExecuteVerification);
        Console.WriteLine("comprobando verificación");
    }

    OnTimedEvent(event)
    {
        Console.WriteLine(event.e.SignalTime.toLocaleTimeString());
    }
}