﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp.C25.IntroEvents.ExampleNoEvents.Models
{
    public class Worker
    {
        public bool IsTired { get; set; }

        public void DoRepairSomething()
        {
            // Reparamos algo y tras un cierto número de reparaciones marcamos al Worker como cansado
            IsTired = true;
        }

        public void DoRoutineChecking()
        {
            var foundSomethingBroken = false;
            var isVandalism = false;

            // aquí hacemos comprobacione que afectan a las variables
            if (foundSomethingBroken)
            {
                if (IsTired)
                {
                    // Primer problema Tenemos que llamar a otro worker que lo repare
                }
                else
                {
                    DoRepairSomething();
                }

                if (isVandalism)
                    CallPolice();
            }
        }

        public void CallPolice()
        {
            //segundo problema, cómo llamamos a la policía?
        }
    }
}
