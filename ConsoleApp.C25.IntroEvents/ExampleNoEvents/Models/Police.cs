﻿using ConsoleApp.C25.IntroEvents.ExampleNoEvents.Models.Events;

namespace ConsoleApp.C25.IntroEvents.ExampleNoEvents.Models
{
    public class Police : ISecurityAgent
    {
        public Worker Worker { get; set; }

        public Police(Worker worker)
        {
            Worker = worker;
        }

        public void DoInvestigate()
        {
            var foundCriminal = false;
            var foundSomethingBroken = false;

            // Todo: aquí hacemos comprobaciones que afectan a las variables

            if (foundCriminal)
            {
                HuntCriminal();
            }

            if (foundSomethingBroken)
            {
                CallWorker();
            }
        }

        public void CallWorker()
        {
            //cómo llamamos al worker?
            Worker.DoRepairSomething();
        }

        public void HuntCriminal()
        {

        }
    }
}
