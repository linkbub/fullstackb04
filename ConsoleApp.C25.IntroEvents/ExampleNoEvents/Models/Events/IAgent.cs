﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp.C25.IntroEvents.ExampleNoEvents.Models.Events
{
    public interface IAgent
    {
        void CallWorker();
    }
}
