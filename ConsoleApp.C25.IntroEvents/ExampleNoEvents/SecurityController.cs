﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleApp.C25.IntroEvents.ExampleNoEvents.Models;
using ConsoleApp.C25.IntroEvents.ExampleNoEvents.Models.Events;

namespace ConsoleApp.C25.IntroEvents.ExampleNoEvents
{
    public class SecurityController
    {
        Police Police1 { get; set; }
        Worker Worker1 { get; set; }

        public SecurityController(Police police, Worker worker)
        {
            Police1 = police;
            Worker1 = worker;
        }

        public void StartWatching()
        {
            bool keepWatching = true;

            while (keepWatching)
            {
                var key = Console.ReadKey().KeyChar;

                if (key == 's')
                {
                    Police1.DoInvestigate();
                }
                else if (key == 'r')
                {
                    Worker1.DoRepairSomething();
                }
                else if (key == 'c')
                {
                    Worker1.DoRoutineChecking();
                }

            }
        }
    }
}
