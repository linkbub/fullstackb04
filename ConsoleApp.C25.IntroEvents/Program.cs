﻿using System;
using System.Timers;

namespace ConsoleApp.C25.IntroEvents
{
    class Program
    {
        #region delegate
        delegate int MyDel(int a, int o);
        delegate T3 MyFunc<T1, T2, T3>(T1 a, T2 b);
        #endregion

        static Timer aTimer = new Timer();

        static void Main(string[] args)
        {
            #region delegate
            
            //MyDel mydel = (a, b) => { return a + b; };
            //Func<int, int, int> myFunc = (a, b) => { return a + b; };

            //HazAlgo(mydel);
            //HazAlgo((a, b) => { return a * b; });
            #endregion

            //primera subscripción
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            //segunda subscripción
            aTimer.Elapsed += new ElapsedEventHandler(ExecuteVerification);

            // esto no tiene nada que ver con los eventos
            aTimer.Interval = 5000;
            aTimer.Enabled = true;

            while(true)
            {

            }

        }

        // Specify what you want to happen when the Elapsed event is raised.
        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {           
            Console.WriteLine(DateTime.Now.ToLongTimeString());
        }

        private static void ExecuteVerification(object source, ElapsedEventArgs e)
        {
            aTimer.Elapsed -= new ElapsedEventHandler(ExecuteVerification);
            Console.WriteLine("comprobando verificación");
        }

        static void HazAlgo(MyDel del)
        {
            var result = del(5, 4);
            Console.WriteLine(del.ToString());
        }
    }
}
