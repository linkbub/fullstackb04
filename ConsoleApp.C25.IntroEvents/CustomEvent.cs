﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp.C25.IntroEvents
{

    public delegate void SecurityBreach(object? sender, SecurityAlertEventArgs e);
    public class SecurityAlertEventArgs
    {
        public SecurityAlertEventArgs(string message)
        {
            Message = message;
        }
        public string Message { get; set; }
    }


    public class BigBrother
    {
        // definimos dos eventos

        // Evento 1: brecha en la seguridad
        public event SecurityBreach Raise_BreachEvent;
        public event EventHandler<SecurityAlertEventArgs> Raise_RandomRoutineCheck_Event;

        public Policia poli { get; set; }

        public void DoSomething()
        {
            // esto no es importante aquí la lógica de negocio
            while (true)
            {
                var isDetectedBreach = false;
                var requestedRandomRoutineChecking = false;

                //.... pasan cosas entre medio que modifican las variables de control


                // do checkings
                if (isDetectedBreach)
                {
                    OnRaised_BreachEvent_CustomEvent(new SecurityAlertEventArgs("Alerta, brecha en la seguridad"));
                    
                    // esto es a saco paco sin evento ni na de na
                    // esto no mola
                    poli.Investigar_Event(this, new SecurityAlertEventArgs("Alerta, brecha en la seguridad"));
                }

                if (requestedRandomRoutineChecking)
                    OnRaised_RandomRoutineCheck_CustomEvent(new SecurityAlertEventArgs("Realizando comprobaciones de rutina"));
            }
        }

        protected virtual void OnRaised_BreachEvent_CustomEvent(SecurityAlertEventArgs evArg)
        {
            //var raiseEvent = Raise_BreachEvent;

            if(Raise_BreachEvent != null)
            {
                // Format the string to send inside the CustomEventArgs parameter
                evArg.Message += $" at {DateTime.Now}";

                // Call to raise the event.
                Raise_BreachEvent(sender: this, e: evArg);
            }
        }

        protected virtual void OnRaised_RandomRoutineCheck_CustomEvent(SecurityAlertEventArgs e)
        {
            var raiseEvent = Raise_BreachEvent;

            if (raiseEvent != null)
            {
                // Format the string to send inside the CustomEventArgs parameter
                e.Message += $" at {DateTime.Now}";

                // Call to raise the event.
                raiseEvent(this, e);
            }
        }
    }

    class PorteroDeEdificio
    {
        public event EventHandler<SecurityAlertEventArgs> Raise_RandomRoutineCheck_Event;

        public void DoSomething()
        {
        }
    }

    public class Policia
    {
        private readonly string _id;

        public Policia(string id, BigBrother pub)
        {
            _id = id;
            pub.Raise_BreachEvent += Investigar_Event;
        }

        public void Investigar_Event(object sender, SecurityAlertEventArgs e)
        {
            // cuando recibo una alerta de seguridad actúo
            Console.WriteLine($"{_id} received this message: {e.Message}");
        }
    }

    class Tecnico
    {
        private readonly string _id;

        public Tecnico(string id, BigBrother pub, PorteroDeEdificio portero = null)
        {
            _id = id;
            // aquí hacemos la subscripción a los mensajes del ayuntamiento
            pub.Raise_RandomRoutineCheck_Event += Handle_RandomRoutineCheck_Event;

            if (portero != null) // aquí hacemos la subscripción a los mensajes del portero del edificio
                portero.Raise_RandomRoutineCheck_Event += Handle_RandomRoutineCheck_Event;
        }

        public void Handle_RandomRoutineCheck_Event(object sender, SecurityAlertEventArgs e)
        {
            Console.WriteLine($"{_id} received this message: {e.Message}");
        }
    }

    class MiProgram
    {
        static void Main2()
        {
            // dos publishers
            var gobierno = new BigBrother();
            var portero = new PorteroDeEdificio();

            // 3 subscriptores
            var poli1 = new Policia("sub1", gobierno);
            var sub2 = new Tecnico("sub2", gobierno);
            var sub3 = new Tecnico("sub3", gobierno, portero);

            gobierno.poli = poli1;

            // Call the method that raises the event.
            gobierno.DoSomething();
            portero.DoSomething();

            // Keep the console window open
            Console.WriteLine("Press any key to continue...");
            Console.ReadLine();
        }
    }
}
