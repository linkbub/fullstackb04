﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp.C25.IntroEvents.ExampleEvents
{
    public interface IWatcher
    {
        event AlertHandler SecurityAlertEvent;
        event AlertHandler RepairAlertEvent;
        event AlertHandler RoutingCheckingAlertEvent;

        void SendAlertRepair(string zona);
        void SendSecurityAlert(string zona);
    }
}
