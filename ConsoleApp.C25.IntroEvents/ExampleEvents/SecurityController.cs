﻿using System;

namespace ConsoleApp.C25.IntroEvents.ExampleEvents
{
    public delegate void AlertHandler(string zona);

    public class SecurityController : IWatcher
    {
        public event AlertHandler SecurityAlertEvent;
        public event AlertHandler RepairAlertEvent;
        public event AlertHandler RoutingCheckingAlertEvent;

        public SecurityController()
        {
        }
               

        public void StartWatching()
        {
            bool keepWatching = true;

            while (keepWatching)
            {
                var key = Console.ReadKey().KeyChar;

                if (key == 's')
                {
                    //Police1.DoInvestigate();
                    Console.WriteLine("Introduzca la zona");
                    var zona = Console.ReadLine();

                    SecurityAlertEvent(zona);
                }
                else if (key == 'r')
                {
                    //Worker1.DoRepairSomething();
                    Console.WriteLine("Introduzca la zona");
                    var zona = Console.ReadLine();

                    RepairAlertEvent(zona);
                }
                else if (key == 'c')
                {
                    //Worker1.DoRoutineChecking();
                    Console.WriteLine("Introduzca la zona");
                    var zona = Console.ReadLine();

                    RoutingCheckingAlertEvent(zona);
                }

            }
        }

        public void SendAlertRepair(string zona)
        {
            Console.WriteLine($"un agente está pidiendo reparaciones en la zona {zona} pulse a para enviar un reparador");
            var key = Console.ReadKey().KeyChar;

            if (key == 'a')
                RepairAlertEvent(zona);
        }

        public void SendSecurityAlert(string zona)
        {
            Console.WriteLine($"un agente está pidiendo investigar un delito en la zona {zona} pulse a para enviar un agente");
            var key = Console.ReadKey().KeyChar;

            if (key == 'a')
                SecurityAlertEvent(zona);
        }
    }
}
