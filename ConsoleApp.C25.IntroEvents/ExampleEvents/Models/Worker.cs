﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp.C25.IntroEvents.ExampleEvents.Models
{
    public class Worker
    {
        IWatcher Watcher;
        public bool IsTired { get; set; }

        public Worker(IWatcher watcher)
        {
            Watcher = watcher;
            Watcher.RepairAlertEvent += DoRepairSomething;
            Watcher.RoutingCheckingAlertEvent += DoRoutineChecking;
                        
            //Watcher.SecurityAlertEvent += DoRoutineChecking;
        }

        public void DoRepairSomething(string zona)
        {
            // Reparamos algo y tras un cierto número de reparaciones marcamos al Worker como cansado
            IsTired = true;
        }

        public void DoRoutineChecking(string zona)
        {
            var foundSomethingBroken = false;
            var isVandalism = false;

            // aquí hacemos comprobacione que afectan a las variables
            if (foundSomethingBroken)
            {
                if (IsTired)
                {
                    // Primer problema Tenemos que llamar a otro worker que lo repare
                }
                else
                {
                    DoRepairSomething(zona);
                }

                if (isVandalism)
                    CallPolice(zona);
            }
        }

        public void CallPolice(string zona)
        {
            //segundo problema, cómo llamamos a la policía?
            Watcher.SendSecurityAlert(zona);
        }
    }
}
