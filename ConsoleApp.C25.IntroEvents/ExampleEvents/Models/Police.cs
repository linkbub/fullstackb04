﻿using ConsoleApp.C25.IntroEvents.ExampleNoEvents.Models.Events;

namespace ConsoleApp.C25.IntroEvents.ExampleEvents.Models
{
    public class Police
    {
        IWatcher Watcher;

        public Police(IWatcher watcher)
        {
            Watcher = watcher;
            Watcher.SecurityAlertEvent += DoInvestigate;
        }

        public void DoInvestigate(string area)
        {
            var foundCriminal = false;
            var foundSomethingBroken = false;

            // Todo: aquí hacemos comprobaciones que afectan a las variables

            if (foundCriminal)
            {
                HuntCriminal();
            }

            if (foundSomethingBroken)
            {
                CallWorker(area);
            }
        }

        public void CallWorker(string area)
        {
            //cómo llamamos al worker?
            Watcher.SendAlertRepair(area);
        }

        public void HuntCriminal()
        {

        }
    }
}
